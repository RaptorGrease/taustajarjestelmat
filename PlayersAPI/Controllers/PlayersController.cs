using System;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using gitTaustajärjestelmät.processors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace gitTaustajärjestelmät.Controllers {
    [Route ("api/players")]
    [ApiController]
    public class PlayersController {
        private readonly PlayersProcessor _proc;

        public PlayersController (PlayersProcessor proc) {
            _proc = proc;

        }

        [HttpGet ("{id:guid}")]
        public Task<Player> GetPlayer (Guid id) { return _proc.GetPlayer (id); }

        [HttpGet ("{name:alpha}")]
        public Task<Player[]> GetPlayerByName (string name) { return _proc.GetPlayerByName (name); }

        [HttpGet]
        public Task<Player[]> GetAllPlayers (int? minScore, string name, int? tagType, string itemProperties, int? size, bool topTen) {
            if (minScore != null) {
                return _proc.GetPlayerList (minScore);
            }
            if (name != null) {
                return _proc.GetPlayerByName (name);
            }
            if (tagType != null) {
                return _proc.GetPlayersByTag (tagType);
            }
            if (itemProperties != null) {
                return _proc.GetPlayersByItemProperties (itemProperties);
            }
            if (size != null) {
                return _proc.GetPlayersByInventorySize (size.GetValueOrDefault ());
            }
            if (topTen == true) {
                return _proc.GetPlayersTenByScore ();
            }

            return _proc.GetAllPlayers ();

        }

        [HttpGet("mostcommonlevel")]
        public Task<BsonDocument[]> GetMostCommonLevel () {

            return _proc.GetMostCommonLevel ();
        }

        [HttpPost]
        public Task<Player> CreatePlayer (NewPlayer player) { return _proc.CreatePlayer (player); }

        [HttpPut ("{id}")]
        public Task<Player> UpdatePlayer (Guid id, ModifiedPlayer player) { return _proc.UpdatePlayer (id, player); }

        [HttpDelete ("{id}")]
        public Task<Player> DeletePlayer (Guid id) { return _proc.DeletePlayer (id); }
    }
}