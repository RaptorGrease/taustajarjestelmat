using System;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using gitTaustajärjestelmät.processors;
using Microsoft.AspNetCore.Mvc;
using PlayersAPI.processors.customExceptions;

namespace PlayersAPI.Controllers
 {
    [Route ("api/players/{playerID}/items")]
    [ApiController]
    public class ItemsController {
        private readonly ItemsProcessor _proc;

        public ItemsController (ItemsProcessor proc) {
            _proc = proc;

        }

        [HttpGet ("{itemID}")]
        public Task<Item> GetItem (Guid id, Guid itemID) { return _proc.GetItem (id, itemID); }

        [HttpGet]
        public Task<Item[]> GetAllItems (Guid playerID) { return _proc.GetAllItems (playerID); }
        [HttpPost]
        [LevelExceptionFilterAttribute]
        public Task<Item> CreateItem (Guid playerID, NewItem item) { return _proc.CreateItem (playerID, item); }

        [HttpPut ("{itemID}")]
        public Task<Item> UpdateItem (Guid playerID, Guid itemID, ModifiedItem item) { return _proc.UpdateItem (playerID, itemID, item); }

        [HttpDelete ("{itemID}")]
        public Task<Item> DeleteItem (Guid playerID, Guid itemID) { return _proc.DeleteItem (playerID, itemID); }
    }
}