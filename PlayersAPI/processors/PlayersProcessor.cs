using System;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using gitTaustajärjestelmät.repositories;
using MongoDB.Bson;

namespace gitTaustajärjestelmät.processors {
    public class PlayersProcessor {
        private readonly IRepository _repo;

        public PlayersProcessor (IRepository repo) {
            _repo = repo;
            if (_repo.GetCount () == 0) {
                CreatePlayer (new NewPlayer () { Name = "Bottington" });
            }
        }
        public Task<Player> GetPlayer (Guid id) { return _repo.GetPlayer (id); }
        public Task<Player[]> GetAllPlayers () { return _repo.GetAllPlayers (); }
        public Task<Player> CreatePlayer (NewPlayer player) {

            Player newPlayer = new Player () { Name = player.Name, Level = player.Level, Id = Guid.NewGuid (), CreationTime = DateTime.UtcNow };
            return _repo.CreatePlayer (newPlayer);

        }
        public Task<Player> UpdatePlayer (Guid id, ModifiedPlayer player) { return _repo.UpdatePlayer (player, id); }
        public Task<Player> DeletePlayer (Guid id) { return _repo.DeletePlayer (id); }
        public Task<Player[]> GetPlayerList (int? minScore) { return _repo.GetPlayerList (minScore); }
        public Task<Player[]> GetPlayerByName (string name) { return _repo.GetPlayerByName (name); }
        public Task<Player[]> GetPlayersByTag (int? tag) { return _repo.GetPlayersByTag (tag); }
        public Task<Player[]> GetPlayersByItemProperties (string itemProperties) {
            ItemProperties property = (ItemProperties) Enum.Parse (typeof (ItemProperties), itemProperties);
            return _repo.GetPlayersByItemProperties (property);
        }
        public Task<Player[]> GetPlayersByInventorySize (int size) { return _repo.GetPlayersByInventorySize (size); }
        public Task<Player[]> GetPlayersTenByScore () { return _repo.GetPlayersTenByScore (); }
        public Task<BsonDocument[]> GetMostCommonLevel () { return _repo.GetMostCommonLevel (); }
    }
}