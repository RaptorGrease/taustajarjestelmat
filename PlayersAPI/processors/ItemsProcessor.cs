using System;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using gitTaustajärjestelmät.repositories;
using Microsoft.Extensions.Logging;
using PlayersAPI.processors.customExceptions;

namespace gitTaustajärjestelmät.processors {
    public class ItemsProcessor {
        private readonly IRepository _repo;
          public ItemsProcessor (IRepository repo, ILogger<ItemsProcessor> logger) {
            _repo = repo;
         
        }
        public Task<Item> GetItem (Guid id, Guid itemID) { return _repo.GetItem (id, itemID); }
        public Task<Item[]> GetAllItems (Guid playerID) { return _repo.GetAllItems (playerID); }
        public Task<Item> CreateItem (Guid playerID, NewItem item) {
           
            if (_repo.GetPlayer (playerID).Result.Level < 3 && item.Type == ItemTypes.Sword) {
                throw new LevelException ("Player does not meet requirements for equipping sword. Level 3 required.");
            }
            Item newItem = new Item () {Level = item.Level, Id = Guid.NewGuid (), Type = item.Type, Properties = item.Properties, CreationTime = DateTime.UtcNow };
            
            return _repo.CreateItem (playerID, newItem);
        }
        public Task<Item> UpdateItem (Guid playerID, Guid itemID, ModifiedItem item) { return _repo.UpdateItem (playerID, itemID, item); }
        public Task<Item> DeleteItem (Guid playerID, Guid itemID) { return _repo.DeleteItem (playerID, itemID); }
    }
}