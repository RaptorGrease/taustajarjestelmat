using System;
using System.ComponentModel.DataAnnotations;
using PlayersAPI.customattributes;

namespace gitTaustajärjestelmät.model {
    public class Item {
        public Guid Id { get; set; }
        public int Level { get; set; }
        public ItemTypes Type { get; set; }
        public DateTime CreationTime { get; set; }
        public ItemProperties Properties { get; set;}
    }

    public class NewItem {
        [Range (1, 99)]
        public int Level { get; set; }
        public ItemTypes Type { get; set; }
        public ItemProperties Properties { get; set;}
    }
    public class ModifiedItem {
        public int Level { get; set; }
        public ItemProperties Properties { get; set;}
    }
    public enum ItemTypes { Sword = 0, Battleaxe, Shield }
    public enum ItemProperties { None = 0, Glowing, Sharp }
}