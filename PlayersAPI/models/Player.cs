using System;
using System.Collections.Generic;

namespace gitTaustajärjestelmät.model {
    public class Player {
        public List<Item> itemList = new List<Item> ();
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public int Level { get; set; }
        public bool IsBanned { get; set; }
        public DateTime CreationTime { get; set; }
        public PlayerTags PlayerTag {get; set;}
        
    }

    public class NewPlayer {
        public string Name { get; set; }
        public int Level { get; set;}
        public PlayerTags PlayerTag {get; set;}
    }
    public class ModifiedPlayer {
        public int Score { get; set; }
         public int Level { get; set;}
         public PlayerTags PlayerTag {get; set;}
         
    }

    public enum PlayerTags {
        pleb = 0, nonCasual
    }

}