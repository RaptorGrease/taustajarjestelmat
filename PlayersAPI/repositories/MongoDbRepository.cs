using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using gitTaustajärjestelmät.repositories;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlayersAPI.repositories {
    public class MongoDbRepository : IRepository {
        private readonly IMongoCollection<Player> _collection;
        public MongoDbRepository () {
            var mongoClient = new MongoClient ("mongodb://localhost:27017");
            IMongoDatabase database = mongoClient.GetDatabase ("Taustajarjestelmat");
            _collection = database.GetCollection<Player> ("Players");
        }
        public async Task<Item> CreateItem (Guid playerId, Item item) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            UpdateDefinition<Player> update = Builders<Player>.Update.Push<Item> (x => x.itemList, item);
            await _collection.UpdateOneAsync (filter, update);
            return item;
        }

        public async Task<Player> CreatePlayer (Player player) {
            await _collection.InsertOneAsync (player);
            return player;
        }

        public async Task<Item> DeleteItem (Guid playerId, Guid itemID) {
            var pull = Builders<Player>.Update.PullFilter (x => x.itemList, y => y.Id == itemID);
            var filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            return (await _collection.FindOneAndUpdateAsync (filter, pull)).itemList.Find (x => x.Id == itemID);
        }

        public Task<Player> DeletePlayer (Guid playerId) {
            var filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            var update = Builders<Player>.Update.Set (x => x.IsBanned, true);
            return _collection.FindOneAndUpdateAsync (filter, update);
        }

        public async Task<Item[]> GetAllItems (Guid playerId) {
            var filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            Player player = await _collection.Find (filter).FirstAsync ();
            return player.itemList.ToArray ();
        }

        public async Task<Player[]> GetAllPlayers () {
            List<Player> players = await _collection.Find (new BsonDocument ()).ToListAsync ();
            return players.ToArray ();
        }
        public async Task<Player[]> GetPlayersTenByScore () {

            List<Player> players = await _collection.Find (new BsonDocument ()).SortByDescending (x => x.Score).Limit (10).ToListAsync ();
            return players.ToArray ();
        }

        public int GetCount () {
            return (int) _collection.Find (new BsonDocument ()).CountDocuments ();
        }

        public async Task<Item> GetItem (Guid playerId, Guid itemId) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            Player player = await _collection.Find (filter).FirstAsync ();
            return player.itemList.Find (x => x.Id == itemId);
        }

        public Task<Player> GetPlayer (Guid playerId) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            return _collection.Find (filter).FirstAsync ();
        }

        public async Task<Player[]> GetPlayerByName (string name) {

            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq (x => x.Name, name);
            Player[] player = new Player[1];
            player[0] = await _collection.Find (filter).FirstAsync ();
            return player;
        }

        public async Task<Player[]> GetPlayersByItemProperties (ItemProperties itemProperties) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.ElemMatch (x => x.itemList, y => y.Properties == itemProperties);
            return (await _collection.Find (filter).ToListAsync ()).ToArray ();
        }

        public async Task<Player[]> GetPlayersByTag (int? tag) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Eq ("PlayerTag", tag);

            return (await _collection.Find (filter).ToListAsync ()).ToArray ();
        }
        public async Task<Player[]> GetPlayersByInventorySize (int size) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Size ("itemList", size);
            return (await _collection.Find (filter).ToListAsync ()).ToArray ();
        }

        public async Task<Player[]> GetPlayerList (int? minScore) {
            FilterDefinition<Player> filter = Builders<Player>.Filter.Gt (x => x.Score, minScore);
            return (await _collection.Find (filter).ToListAsync ()).ToArray ();
        }

        public async Task<Item> UpdateItem (Guid playerId, Guid itemID, ModifiedItem item) {
            var filter = Builders<Player>.Filter.And (Builders<Player>.Filter.Where (x => x.Id == playerId),
                Builders<Player>.Filter.ElemMatch (x => x.itemList, y => y.Id == itemID));
            var update = Builders<Player>.Update.Set ("itemList.$.Level", item.Level).Set ("itemProperties", item.Properties);
            await _collection.UpdateOneAsync (filter, update);
            return await GetItem (playerId, itemID);
        }

        public async Task<Player> UpdatePlayer (ModifiedPlayer player, Guid playerId) {
            var filter = Builders<Player>.Filter.Eq (x => x.Id, playerId);
            var update = Builders<Player>.Update.Set (x => x.Score, player.Score).Set (x => x.Level, player.Level).Set (x => x.PlayerTag, player.PlayerTag);
            await _collection.UpdateOneAsync (filter, update);
            return await _collection.Find (filter).FirstAsync ();
        }

        public async Task<BsonDocument[]> GetMostCommonLevel () {
            return (await _collection.Aggregate ().Project ("{ Level: 1 }")
                .Group ("{'_id' : '$Level', 'count' : {'$sum' : 1}}")
                .Sort ("{count: -1}")
                .Limit (2).ToListAsync ()).ToArray ();
        }
    }
}