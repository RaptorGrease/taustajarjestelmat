using System;
using System.Threading.Tasks;
using gitTaustajärjestelmät.model;
using MongoDB.Bson;

namespace gitTaustajärjestelmät.repositories {
    public interface IRepository {
        int GetCount ();
        Task<Player> CreatePlayer (Player player);
        Task<Player> GetPlayer (Guid playerId);
        Task<Player[]> GetAllPlayers ();
        Task<Player> UpdatePlayer (ModifiedPlayer player, Guid playerId);
        Task<Player> DeletePlayer (Guid playerId);

        Task<Item> CreateItem (Guid playerId, Item item);
        Task<Item> GetItem (Guid playerId, Guid itemId);
        Task<Item[]> GetAllItems (Guid playerId);
        Task<Item> UpdateItem (Guid playerId, Guid itemID, ModifiedItem item);
        Task<Item> DeleteItem (Guid playerId, Guid itemID);
        Task<Player[]> GetPlayerList (int? minScore);
        Task<Player[]> GetPlayerByName (string name);
        Task<Player[]> GetPlayersByTag (int? tag);
        Task<Player[]> GetPlayersByItemProperties (ItemProperties itemProperties);
        Task<Player[]> GetPlayersByInventorySize (int size);
        Task<Player[]> GetPlayersTenByScore ();
        Task<BsonDocument[]> GetMostCommonLevel ();
    }
}