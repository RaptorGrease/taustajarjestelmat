using System;
using System.ComponentModel.DataAnnotations;

namespace PlayersAPI.customattributes
{
    public class MyDateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object val, ValidationContext validationContext){
            DateTime d = (DateTime)val;
            if(d > DateTime.UtcNow){
                return new ValidationResult("Datetime must be from the past.");
            }
            return ValidationResult.Success;
        }
    }
}