using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PlayersAPI.processors.customExceptions {
    public class LevelException : Exception {
        public LevelException (string message) : base(message) {
           
        }
    }
    public class LevelExceptionFilterAttribute : ExceptionFilterAttribute {
       
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is LevelException) {
                context.Result = new StatusCodeResult(403);
            }
        }
    }
}



    